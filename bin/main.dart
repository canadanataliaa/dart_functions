num add(num x, num y) {
    return x + y;
}

Map<String, num> switchNumber(num num1, num2) {
    return {
        'num1': num2,
        'num2': num1
    };
}

int? countLetter(String letter, String sentence) {
    int count = 0;

    for (var i=0; i < sentence.length; i++) {
        if (sentence[i] == letter) {
            count++;
        } else if (sentence[i].contains(letter)) {
            return count;
        }
    }
    
    return count > 0 ? count : null;
}

bool isPalindrome(String text) {
    text = text.toLowerCase().replaceAll(' ', '');
    var reversed = '';

    for (var i=text.length-1; i>=0; i--) {
        reversed += text[i];
    }

    if (reversed == text) {
        return true;
    }
    return false;
}

bool isIsogram(String text) {
    Set letters = text.toLowerCase().split('').toSet();

    if (letters.length == text.length) {
        return true;
    }
    return false;
}

num? purchase(int age, num price) {
    num discountedPrice = price * 0.8;
    num roundedPrice = num.parse(discountedPrice.toStringAsFixed(2));

    if (age < 13) {
        return null;
    } else if (age > 13 && age < 21) {
        return roundedPrice;
    } else if (age >= 65) {
        return roundedPrice;
    } else {
        return num.parse(price.toStringAsFixed(2));
    }
}

List<String> findHotCategories(List items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    Set hotCategories = {};

    items.forEach((category) {
        if (category['stocks'] == 0) {
            hotCategories.add(category['category']);
        }
    });

    return [...hotCategories];
}

List<String> findFlyingVoters(List candidateA, List candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    List voters = [];

    for (String voterA in candidateA) {
        for (String voterB in candidateB) {
            if (voterA == voterB) {
                voters.add(voterA);
            }
        }
    }

    return [...voters];
}

List<Map<String, dynamic>> calculateAdEfficiency(List items) {
    // Sort the ad efficiency according to the most to least efficient.

    // The passed campaigns array from the test are the following:
    // { brand: 'Brand X', expenditure: 12345.89, customersGained: 4879 }
    // { brand: 'Brand Y', expenditure: 22456.17, customersGained: 6752 }
    // { brand: 'Brand Z', expenditure: 18745.36, customersGained: 5823 }

    // The efficiency is computed as (customersGained / expenditure) x 100.

    // The expected output after processing the campaigns array are:
    // { brand: 'Brand X', adEfficiency: 39.51922461645131 }
    // { brand: 'Brand Z', adEfficiency: 31.063687227132476 }
    // { brand: 'Brand Y', adEfficiency: 30.06746030155632 }

    List campaignEfficiencies = [];

    items.forEach((campaign) {
        var efficiency = (campaign['customersGained'] / campaign['expenditure']) * 100;

        campaignEfficiencies.add({
            'brand': campaign['brand'],
            'adEfficiency': efficiency
        });
    });

    campaignEfficiencies.sort((a,b) => b['adEfficiency'].compareTo(a['adEfficiency']));

    return [...campaignEfficiencies];
}